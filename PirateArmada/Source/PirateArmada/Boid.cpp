// Fill out your copyright notice in the Description page of Project Settings.


#include "Boid.h"
#include "Components/SphereComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "DrawDebugHelpers.h"
#include "kDOP.h"
#include "ShipSpawner.h"
#include "Engine/StaticMeshActor.h"

// Sets default values
ABoid::ABoid()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//setup boid mesh component & attach to root
	BoidMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Boid Mesh Component"));
	RootComponent = BoidMesh;
	BoidMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	BoidMesh->SetCollisionResponseToAllChannels(ECR_Ignore);

	//setup boid collision component and set as root
	BoidCollision = CreateDefaultSubobject<USphereComponent>(TEXT("Boid Collision Component"));
	BoidCollision->SetupAttachment(RootComponent);
	BoidCollision->SetCollisionObjectType(ECC_Pawn);
	BoidCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	BoidCollision->SetCollisionResponseToAllChannels(ECR_Overlap);
	BoidCollision->SetSphereRadius(30.0f);

	//setup cohesion sensor component
	PerceptionSensor = CreateDefaultSubobject<USphereComponent>(TEXT("Perception Sensor Component"));
	PerceptionSensor->SetupAttachment(RootComponent);
	PerceptionSensor->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	PerceptionSensor->SetCollisionResponseToAllChannels(ECR_Ignore);
	PerceptionSensor->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	PerceptionSensor->SetSphereRadius(300.0f);

	//set default velocity
	BoidVelocity = FVector::ZeroVector;

	//empty sensor array
	AvoidanceSensors.Empty();

	//theta angle of rotation on xy plane around z axis (yaw) around sphere

	//direction vector pointing from the center to point on sphere surface
	FVector SensorDirection;

	for (int32 i = 0; i < NumSensors; ++i)
	{
		//calculate the spherical coordinates of the direction vectors endpoint
		float yaw = 2 * UKismetMathLibrary::GetPI() * GoldenRatio * i;
		float pitch = FMath::Acos(1 - (2 * float(i) / NumSensors));

		//convert point on sphere to cartesian coordinates xyz
		SensorDirection.X = FMath::Cos(yaw) * FMath::Sin(pitch);
		SensorDirection.Y = FMath::Sin(yaw) * FMath::Sin(pitch);
		SensorDirection.Z = FMath::Cos(pitch);
		//add direction to list of sensors for avoidance
		AvoidanceSensors.Emplace(SensorDirection);
	}

	IsCollectingGold = false;
	IsAlive = true;
}

// Called when the game starts or when spawned
void ABoid::BeginPlay()
{
	Super::BeginPlay();
	//set velocity based on spawn rotation and flock speed settings
	BoidVelocity = this->GetActorForwardVector();
	BoidVelocity.Normalize();
	BoidVelocity *= FMath::FRandRange(MinSpeed, MaxSpeed);

	BoidCollision->OnComponentBeginOverlap.AddDynamic(this, &ABoid::OnHitboxOverlapBegin);
	BoidCollision->OnComponentEndOverlap.AddDynamic(this, &ABoid::OnHitboxOverlapEnd);

	//set current mesh rotation
	CurrentRotation = this->GetActorRotation();
}

// Called every frame
void ABoid::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//move ship
	FlightPath(DeltaTime);

	//update ship mesh rotation
	UpdateMeshRotation();

	if (CollisionCloud != nullptr)
	{
		GoldCollected += CollisionCloud->RemoveGold();
	}

	if (Invincibility > 0)
	{
		Invincibility -= DeltaTime;
	}

	// if ship is live, add fitness each frame.
	if (IsAlive)
	{
		/*fitness ++;
		shipDNA.m_storedFitness = fitness;*/
		ShipDNA.m_storedFitness++;
	}
}

void ABoid::UpdateMeshRotation()
{
	//rotate toward current boid heading smoothly
	CurrentRotation = FMath::RInterpTo(CurrentRotation, this->GetActorRotation(), GetWorld()->DeltaTimeSeconds, 7.0f);
	this->BoidMesh->SetWorldRotation(CurrentRotation);
}

//-------------------------------------Lab 8 Starts Here-----------------------------------------------------------------------------

void ABoid::FlightPath(float DeltaTime)
{
	FVector Acceleration = FVector::ZeroVector;

	//update position and rotation
	this->SetActorLocation(this->GetActorLocation() + (BoidVelocity * DeltaTime));
	this->SetActorRotation(BoidVelocity.ToOrientationQuat());

	TArray<AActor*> NearbyShips;
	PerceptionSensor->GetOverlappingActors(NearbyShips, TSubclassOf<ABoid>());
	Acceleration += AvoidShips(NearbyShips);
	Acceleration += VelocityMatching(NearbyShips);
	Acceleration += FlockCentering(NearbyShips);
	if (IsObstacleAhead())
		Acceleration += AvoidObstacle();

	//apply gas cloud forces
	for (int i = 0; i < Spawner->GasClouds.Num(); i++)
	{
		FVector Force = Spawner->GasClouds[i]->GetActorLocation() - GetActorLocation();
		if (Force.Size() < 1500)
			GasCloudForces.Add(Force);
	}

	for (FVector GasCloudForce : GasCloudForces)
	{
		GasCloudForce = GasCloudForce.GetSafeNormal() * GasCloudStrength;
		Acceleration += GasCloudForce;
		GasCloudForces.Remove(GasCloudForce);
	}

	//update velocity
	BoidVelocity += (Acceleration * DeltaTime);
	BoidVelocity = BoidVelocity.GetClampedToSize(MinSpeed, MinSpeed + SpeedStrength / 10000 * 300);
}

FVector ABoid::AvoidShips(TArray<AActor*> NearbyShips)
{
	FVector Steering = FVector::ZeroVector;
	int ShipCount = 0;
	FVector SeparationDirection = FVector::ZeroVector;
	float ProximityFactor = 0.0f;

	for (AActor* OverlapActor : NearbyShips)
	{
		ABoid* LocalShip = Cast<ABoid>(OverlapActor);
		if (LocalShip != nullptr && LocalShip != this)
		{
			// check if LocalShip is outside perception fov
			if (FVector::DotProduct(this->GetActorForwardVector(),
			                        (LocalShip->GetActorLocation() - this->GetActorLocation()).GetSafeNormal()) <=
				SeparationFOV)
			{
				continue; //LocalShip is outside the perception, disregard it and continue the loop
			}

			// get normalized direction from nearby ship
			SeparationDirection = this->GetActorLocation() - LocalShip->GetActorLocation();
			SeparationDirection = SeparationDirection.GetSafeNormal() * 2;

			// get scaling factor based off other ship's proximity. 0 = far away & 1 = very close
			ProximityFactor = 1.0f - (SeparationDirection.Size() / this->PerceptionSensor->GetScaledSphereRadius());

			// add steering force of ship and increase flock count
			Steering += (ProximityFactor * SeparationDirection);
			ShipCount++;
		}
	}

	if (ShipCount > 0)
	{
		// get flock overage separation steering force, apply separation strength factor and return force
		Steering /= ShipCount;
		Steering.GetSafeNormal() -= this->BoidVelocity.GetSafeNormal();
		Steering *= SeparationStrength;
		return Steering;
	}
	else
	{
		return FVector::ZeroVector;
	}
}

FVector ABoid::VelocityMatching(TArray<AActor*> NearbyShips)
{
	FVector Steering = FVector::ZeroVector;
	int ShipCount = 0.0f;

	for (AActor* OverlapActor : NearbyShips)
	{
		ABoid* LocalShip = Cast<ABoid>(OverlapActor);
		if (LocalShip != nullptr && LocalShip != this)
		{
			if (FVector::DotProduct(this->GetActorForwardVector(),
			                        (LocalShip->GetActorLocation() - this->GetActorLocation()).GetSafeNormal()) <=
				AlignmentFOV)
			{
				continue; // LocalShip is outside viewing angle, disregard it and continue the loop
			}
			// add LocalShip's alignment force
			Steering += LocalShip->BoidVelocity.GetSafeNormal();
			ShipCount++;
		}
	}

	if (ShipCount > 0)
	{
		// get steering force to average flocking direction
		Steering /= ShipCount;
		Steering.GetSafeNormal() -= this->BoidVelocity.GetSafeNormal();
		Steering *= VelocityStrength;
		return Steering;
	}
	else
	{
		return FVector::ZeroVector;
	}
}

FVector ABoid::FlockCentering(TArray<AActor*> NearbyShips)
{
	FVector Steering = FVector::ZeroVector;
	int ShipCount = 0;
	FVector AveragePosition = FVector::ZeroVector;

	for (AActor* OVerlapActor : NearbyShips)
	{
		ABoid* LocalShip = Cast<ABoid>(OVerlapActor);
		if (LocalShip != nullptr && LocalShip != this)
		{
			if (FVector::DotProduct(this->GetActorForwardVector(),
			                        (LocalShip->GetActorLocation() - this->GetActorLocation()).GetSafeNormal()) <=
				CohesionFOV)
			{
				continue; //LocalShip is outside viewing angle, disregard this LocalShip and continue the loop
			}
			// get cohesive force to group with LocalShip
			AveragePosition += LocalShip->GetActorLocation();
			ShipCount++;
		}
	}
	if (ShipCount > 0)
	{
		// average cohesion force of Ships
		AveragePosition /= ShipCount;
		Steering = AveragePosition - this->GetActorLocation();
		Steering.GetSafeNormal() -= this->BoidVelocity.GetSafeNormal();
		Steering *= CenteringStrength;
		return Steering;
	}
	else
	{
		return FVector::ZeroVector;
	}
}

FVector ABoid::AvoidObstacle()
{
	FVector Steering = FVector::ZeroVector;
	FQuat SensorRotation = FQuat::FindBetweenVectors(AvoidanceSensors[0], this->GetActorForwardVector());
	FVector NewSensorDirection = FVector::ZeroVector;
	FCollisionQueryParams TraceParameters;
	FHitResult Hit;

	for (FVector AvoidanceSensor : AvoidanceSensors)
	{
		// Trace check
		NewSensorDirection = SensorRotation.RotateVector(AvoidanceSensor);
		GetWorld()->LineTraceSingleByChannel(
			Hit,
			this->GetActorLocation(),
			this->GetActorLocation() + NewSensorDirection * SensorRadius,
			ECC_GameTraceChannel1,
			TraceParameters);

		if (!Hit.bBlockingHit)
		{
			Steering = NewSensorDirection.GetSafeNormal() - this->BoidVelocity.GetSafeNormal();
			Steering *= AvoidanceStrength;
			return Steering;
		}
	}
	return FVector::ZeroVector;
}


//------------------------------------------Collision Code---------------------------------------------------------------------------


bool ABoid::IsObstacleAhead()
{
	if (AvoidanceSensors.Num() > 0)
	{
		FQuat SensorRotation = FQuat::FindBetweenVectors(AvoidanceSensors[0], this->GetActorForwardVector());
		FVector NewSensorDirection = FVector::ZeroVector;
		NewSensorDirection = SensorRotation.RotateVector(AvoidanceSensors[0]);
		FCollisionQueryParams TraceParameters;
		FHitResult Hit;
		//line trace
		GetWorld()->LineTraceSingleByChannel(Hit,
		                                     this->GetActorLocation(),
		                                     this->GetActorLocation() + NewSensorDirection * SensorRadius,
		                                     ECC_GameTraceChannel1,
		                                     TraceParameters);

		//check if boid is inside object (i.e. no need to avoid/impossible to)
		if (Hit.bBlockingHit)
		{
			TArray<AActor*> OverlapActors;
			BoidCollision->GetOverlappingActors(OverlapActors);
			for (AActor* OverlapActor : OverlapActors)
			{
				if (Hit.Actor == OverlapActor)
				{
					return false;
				}
			}
		}
		return Hit.bBlockingHit;
	}
	return false;
}

void ABoid::OnHitboxOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                 UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep,
                                 const FHitResult& SweepResult)
{
	if (OtherActor && OtherActor != this)
	{
		// hit other Boids
		if (Invincibility <= 0 &&
			OtherComponent->GetName().Equals(TEXT("Boid Collision Component")))
		{
			AGasCloud* cloud = Cast<AGasCloud>(OtherActor);
			if (cloud != nullptr)
			{
				CollisionCloud = cloud;
				return;
			}

			ABoid* ship = Cast<ABoid>(OtherActor);
			if (ship != nullptr)
			{
				Spawner->NumOfShips--;
				// ship dead
				IsAlive = false;
				// when alteration equal to 1, it means killed by hitting other ships
				CalculateAndStoreFitness(1);
				Spawner->deadDNA.Add(ShipDNA);
				Destroy();
				return;
			}
		}
		// hit Cube Wall
		if (OtherActor->GetName().Contains("Cube") &&
			OverlappedComponent->GetName().Equals(TEXT("Boid Collision Component")))
		{
			Spawner->NumOfShips--;
			// when alteration equal to 2, it means killed by hitting Wall
			CalculateAndStoreFitness(2);
			Spawner->deadDNA.Add(ShipDNA);
			IsAlive = false;
			Destroy();
		}
		// killed by Predator
		if (OtherActor->GetName().Contains("Pirate") &&
			OverlappedComponent->GetName().Equals(TEXT("Boid Collision Component")))
		{
			// when alteration equal to 3, it means killed by hitting Wall
			CalculateAndStoreFitness(3);
			Spawner->deadDNA.Add(ShipDNA);
			IsAlive = false;
			Destroy();
		}
		// collect gold
		if (OtherActor->GetName().Contains("GasCloud") &&
			OverlappedComponent->GetName().Equals(TEXT("Boid Collision Component")))
		{
			AGasCloud* cloud = Cast<AGasCloud>(OtherActor);
			if (cloud != nullptr)
			{
				CollisionCloud = cloud;
				return;
			}
			// the gold storage will decrease
			cloud->RemoveGold();
			// if collecting
			IsCollectingGold = true;
			// when alteration equal to 3, it means collecting gold
			CalculateAndStoreFitness(4);
		}
	}
}

void ABoid::OnHitboxOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                               UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex)
{
	AGasCloud* cloud = Cast<AGasCloud>(OtherActor);
	if (cloud != nullptr)
	{
		CollisionCloud = nullptr;
	}
	// stop collecting after leave the range. 
	IsCollectingGold = false;
}

void ABoid::SetDNA()
{
	VelocityStrength = ShipDNA.StrengthValues[0];
	SeparationStrength = ShipDNA.StrengthValues[1];
	CenteringStrength = ShipDNA.StrengthValues[2];
	AvoidanceStrength = ShipDNA.StrengthValues[3];
	GasCloudStrength = ShipDNA.StrengthValues[4];
	SpeedStrength = ShipDNA.StrengthValues[5];
}

void ABoid::CalculateAndStoreFitness(int alteration)
{
	if (ShipDNA.m_storedFitness == -1)
	{
		// first time calculating fitness
		ShipDNA.m_storedFitness = 0;

		// Add Fitness calculation Here
		// if hit other boids
		if (alteration == 1)
			// it will receive a 25% reduction in fitness.
			ShipDNA.m_storedFitness -= ShipDNA.m_storedFitness * 0.25;
		// if hit walls
		else if (alteration == 2)
			// it will receive a 25% reduction in fitness.
			ShipDNA.m_storedFitness -= ShipDNA.m_storedFitness * 0.25;
		// if hit predator
		else if (alteration == 3)
			// it will receive a 50% reduction in fitness.
			ShipDNA.m_storedFitness -= ShipDNA.m_storedFitness * 0.5;
		// if collect Gold 
		else if (alteration == 4)
			// add one fitness per gold. GoldCollected * 10 = 0.1 * 10 = 1 
			ShipDNA.m_storedFitness += GoldCollected * 10;
		/*else
			fitness = shipDNA.m_storedFitness;	*/
	}
	//if already calculate
	else
	{
		// if collect gold
		if (alteration == 4)
			ShipDNA.m_storedFitness += GoldCollected * 10;
		Fitness = ShipDNA.m_storedFitness;
	}
}
