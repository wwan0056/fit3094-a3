// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Boid.h"
#include "GasCloud.h"
#include "GameFramework/Actor.h"
#include "ShipSpawner.generated.h"

UCLASS()
class PIRATEARMADA_API AShipSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AShipSpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, Category = "Entities")
	float MaxShipCount = 600;

	UPROPERTY(EditAnywhere, Category = "Entities")
	TSubclassOf<ABoid> HarvestShip;
	
	UPROPERTY(EditAnywhere, Category = "Entities")
	TSubclassOf<AGasCloud> GasCloud;

	int NumOfShips = 0;
	TArray<AGasCloud*> GasClouds;

	void SpawnShip();
	void SetShipVariables(ABoid* Ship);
	
	TArray<DNA> m_population;
	TArray<DNA> deadDNA;

	int m_numberOfGenerations=0;
	int m_highestFitness = 0;
	int NUM_PARENTS_PAIR = 50;
	float MUTATION_CHANCE = 0.10f;
	void GeneratePopulation(TArray<DNA> newChildren = TArray<DNA>());
	TArray<DNA> ChildGeneration();
};
