// Fill out your copyright notice in the Description page of Project Settings.


#include "ShipSpawner.h"

#include "Predator.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AShipSpawner::AShipSpawner()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AShipSpawner::BeginPlay()
{
	Super::BeginPlay();
	GeneratePopulation();
	for (int i = 0; i < MaxShipCount; i++)
	{
		SpawnShip();
	}

	for (int i = 0; i < 5; i++)
	{
		float xloc = FMath::RandRange(-2500.0f, 2500.0f);
		float yloc = FMath::RandRange(-2500.0f, 2500.0f);
		float zloc = FMath::RandRange(0.0f, 5000.0f);
		FVector loc(xloc, yloc, zloc);
		GasClouds.Add(Cast<AGasCloud>(GetWorld()->SpawnActor(GasCloud, &loc, &FRotator::ZeroRotator)));
	}
}

// Called every frame
void AShipSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	// spawn after 80% harvester ship dead
	if (NumOfShips < MaxShipCount * .2)
	{
		TArray<AActor*> AliveHarvesters;
		UGameplayStatics::GetAllActorsOfClass(GetWorld(), ABoid::StaticClass(),
		                                      AliveHarvesters);
		
		// Generate the Next Generation
		// We have 2 lists (alive ships with DNA and a list of dead DNA)
		// ...
		
		m_population.Empty();
		for (auto Ship : AliveHarvesters)
		{
			m_population.Add(Cast<ABoid>(Ship)->ShipDNA);
			//Cast<ABoid>(Ship)->CalculateAndStoreFitness(0);
			
		}
		m_population.Append(deadDNA);
		deadDNA.Empty();

		GeneratePopulation(ChildGeneration());

		for (auto Ship : AliveHarvesters)
		{
			Cast<ABoid>(Ship)->ShipDNA = m_population[0];
			m_population.RemoveAt(0);
			Cast<ABoid>(Ship)->SetDNA();
		}

		while (NumOfShips < MaxShipCount)
		{
			SpawnShip();
		}
	}
}

void AShipSpawner::SpawnShip()
{
	float xloc = FMath::RandRange(-2000.0f, 2000.0f);
	float yloc = FMath::RandRange(-2000.0f, 2000.0f);
	float zloc = FMath::RandRange(500.0f, 4500.0f);
	FVector loc(xloc, yloc, zloc);
	ABoid* SpawnedShip = Cast<ABoid>(GetWorld()->SpawnActor(HarvestShip, &loc, &FRotator::ZeroRotator));
	SpawnedShip->Spawner = this;
	SetShipVariables(SpawnedShip);
	NumOfShips++;
}

void AShipSpawner::SetShipVariables(ABoid* Ship)
{
	if (m_population.Num() > 0)
	{
		Ship->ShipDNA = m_population[0];
		m_population.RemoveAt(0);
		Ship->SetDNA();
	}
	else
	{
		// if not enough DNA in m_population
		Ship->ShipDNA = DNA(6);
		Ship->SetDNA();
	}
}

void AShipSpawner::GeneratePopulation(TArray<DNA> newChildren)
{
	if (newChildren.Num() == 0)
	{
		for (int i = 0; i < MaxShipCount; ++i)
		{
			m_population.Add(DNA(6));
		}
	}
	else
	{
		m_population.Empty();
		m_population.Append(newChildren);

		for (int i = 0; i < MaxShipCount - newChildren.Num(); ++i)
		{
			m_population.Add(DNA(6));
		}
	}
	m_numberOfGenerations++;
}

TArray<DNA> AShipSpawner::ChildGeneration()
{
	TArray<DNA> parents;

	for (int i = 0; i < NUM_PARENTS_PAIR * 2; ++i)
	{
		int highestFitness = 0;
		int dnaIdx = -1;
		for (int j = 0; j < m_population.Num(); ++j)
		{
			if (m_population[j].m_storedFitness > highestFitness)
			{
				highestFitness = m_population[j].m_storedFitness;
				dnaIdx = j;
			}
		}
		if (dnaIdx != -1)
		{
			// found our highest fitness dna! store the parent
			parents.Add(m_population[dnaIdx]);
			// remove parent from population
			m_population.RemoveAt(dnaIdx);
			
			
		}
	}
	// create children
	// set up array of children to be added to new population
	TArray<DNA> newChildren;

	for (int i = 0; i < NUM_PARENTS_PAIR; ++i)
	{
		DNA childOne = parents[i * 2 + 0].Crossover(parents[1]);
		DNA childTwo = parents[i * 2 + 1].Crossover(parents[0]);

		// possibly mutate them
		if (FMath::RandRange(0.0f, 1.0f) < MUTATION_CHANCE)
		{
			if (FMath::RandBool())
				childOne.Mutation();
			else
				childTwo.Mutation();
		}
		newChildren.Add(childOne);
		newChildren.Add(childTwo);
	}
	return newChildren;
}
