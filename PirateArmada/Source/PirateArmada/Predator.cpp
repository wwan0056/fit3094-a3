// Fill out your copyright notice in the Description page of Project Settings.


#include "Predator.h"

#include "PredatorSpawner.h"
#include "ShipSpawner.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
APredator::APredator()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//setup Predator mesh component & attach to root
	PredatorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Predator Mesh Component"));
	RootComponent = PredatorMesh;
	PredatorMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	PredatorMesh->SetCollisionResponseToAllChannels(ECR_Ignore);

	//setup Predator collision component and set as root
	PredatorCollision = CreateDefaultSubobject<USphereComponent>(TEXT("Predator Collision Component"));
	PredatorCollision->SetupAttachment(RootComponent);
	PredatorCollision->SetCollisionObjectType(ECC_Pawn);
	PredatorCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	PredatorCollision->SetCollisionResponseToAllChannels(ECR_Overlap);
	PredatorCollision->SetSphereRadius(30.0f);

	//setup cohesion sensor component
	PerceptionSensor = CreateDefaultSubobject<USphereComponent>(TEXT("Perception Sensor Component"));
	PerceptionSensor->SetupAttachment(RootComponent);
	PerceptionSensor->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	PerceptionSensor->SetCollisionResponseToAllChannels(ECR_Ignore);
	PerceptionSensor->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	PerceptionSensor->SetSphereRadius(300.0f);

	//set default velocity
	PredatorVelocity = FVector::ZeroVector;

	//empty sensor array
	AvoidanceSensors.Empty();

	//theta angle of rotation on xy plane around z axis (yaw) around sphere

	//direction vector pointing from the center to point on sphere surface
	FVector SensorDirection;

	for (int32 i = 0; i < NumSensors; ++i)
	{
		//calculate the spherical coordinates of the direction vectors endpoint
		float yaw = 2 * UKismetMathLibrary::GetPI() * GoldenRatio * i;
		float pitch = FMath::Acos(1 - (2 * float(i) / NumSensors));

		//convert point on sphere to cartesian coordinates xyz
		SensorDirection.X = FMath::Cos(yaw) * FMath::Sin(pitch);
		SensorDirection.Y = FMath::Sin(yaw) * FMath::Sin(pitch);
		SensorDirection.Z = FMath::Cos(pitch);
		//add direction to list of sensors for avoidance
		AvoidanceSensors.Emplace(SensorDirection);
	}

	IsAlive = true;
	IsAttacking = false;
}

// Called when the game starts or when spawned
void APredator::BeginPlay()
{
	Super::BeginPlay();
	//set velocity based on spawn rotation and flock speed settings
	PredatorVelocity = this->GetActorForwardVector();
	PredatorVelocity.Normalize();
	PredatorVelocity *= PredatorSpeed;

	PredatorCollision->OnComponentBeginOverlap.AddDynamic(this, &APredator::OnHitboxOverlapBegin);
	PredatorCollision->OnComponentEndOverlap.AddDynamic(this, &APredator::OnHitboxOverlapEnd);

	//set current mesh rotation
	CurrentRotation = this->GetActorRotation();
}

// Called every frame
void APredator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	//move ship
	FlightPath(DeltaTime);

	//update ship mesh rotation
	UpdateMeshRotation();

	/*if (CollisionCloud != nullptr)
	{
		GoldCollected += CollisionCloud->RemoveGold();
	}*/

	if (Invincibility > 0)
	{
		Invincibility -= DeltaTime;
	}

	if (IsAlive)
	{
		/*fitness ++;
		shipDNA.m_storedFitness = fitness;*/
		ShipDNA.m_storedFitness++;
		Fitness = ShipDNA.m_storedFitness;
	}
}

void APredator::UpdateMeshRotation()
{
	//rotate toward current Predator heading smoothly
	CurrentRotation = FMath::RInterpTo(CurrentRotation, this->GetActorRotation(), GetWorld()->DeltaTimeSeconds, 7.0f);
	this->PredatorMesh->SetWorldRotation(CurrentRotation);
}

FVector APredator::AvoidShips(TArray<AActor*> NearbyShips)
{
	FVector Steering = FVector::ZeroVector;
	int ShipCount = 0;
	FVector SeparationDirection = FVector::ZeroVector;
	float ProximityFactor = 0.0f;

	for (AActor* OverlapActor : NearbyShips)
	{
		APredator* LocalShip = Cast<APredator>(OverlapActor);
		if (LocalShip != nullptr && LocalShip != this)
		{
			// check if LocalShip is outside perception fov
			if (FVector::DotProduct(this->GetActorForwardVector(),
			                        (LocalShip->GetActorLocation() - this->GetActorLocation()).GetSafeNormal()) <=
				SeparationFOV)
			{
				continue; //LocalShip is outside the perception, disregard it and continue the loop
			}

			// get normalized direction from nearby ship
			SeparationDirection = this->GetActorLocation() - LocalShip->GetActorLocation();
			SeparationDirection = SeparationDirection.GetSafeNormal() * 2;

			// get scaling factor based off other ship's proximity. 0 = far away & 1 = very close
			ProximityFactor = 1.0f - (SeparationDirection.Size() / this->PerceptionSensor->GetScaledSphereRadius());

			// add steering force of ship and increase flock count
			Steering += (ProximityFactor * SeparationDirection);
			ShipCount++;
		}
	}

	if (ShipCount > 0)
	{
		// get flock overage separation steering force, apply separation strength factor and return force
		Steering /= ShipCount;
		Steering.GetSafeNormal() -= this->PredatorVelocity.GetSafeNormal();
		Steering *= SeparationStrength;
		return Steering;
	}
	else
	{
		return FVector::ZeroVector;
	}
}

FVector APredator::VelocityMatching(TArray<AActor*> NearbyShips)
{
	FVector Steering = FVector::ZeroVector;
	int ShipCount = 0.0f;

	for (AActor* OverlapActor : NearbyShips)
	{
		APredator* LocalShip = Cast<APredator>(OverlapActor);
		if (LocalShip != nullptr && LocalShip != this)
		{
			if (FVector::DotProduct(this->GetActorForwardVector(),
			                        (LocalShip->GetActorLocation() - this->GetActorLocation()).GetSafeNormal()) <=
				AlignmentFOV)
			{
				continue; // LocalShip is outside viewing angle, disregard it and continue the loop
			}
			// add LocalShip's alignment force
			Steering += LocalShip->PredatorVelocity.GetSafeNormal();
			ShipCount++;
		}
	}

	if (ShipCount > 0)
	{
		// get steering force to average flocking direction
		Steering /= ShipCount;
		Steering.GetSafeNormal() -= this->PredatorVelocity.GetSafeNormal();
		Steering *= VelocityStrength;
		return Steering;
	}
	else
	{
		return FVector::ZeroVector;
	}
}

FVector APredator::FlockCentering(TArray<AActor*> NearbyShips)
{
	FVector Steering = FVector::ZeroVector;
	int ShipCount = 0;
	FVector AveragePosition = FVector::ZeroVector;

	for (AActor* OVerlapActor : NearbyShips)
	{
		APredator* LocalShip = Cast<APredator>(OVerlapActor);
		if (LocalShip != nullptr && LocalShip != this)
		{
			if (FVector::DotProduct(this->GetActorForwardVector(),
			                        (LocalShip->GetActorLocation() - this->GetActorLocation()).GetSafeNormal()) <=
				CohesionFOV)
			{
				continue; //LocalShip is outside viewing angle, disregard this LocalShip and continue the loop
			}
			// get cohesive force to group with LocalShip
			AveragePosition += LocalShip->GetActorLocation();
			ShipCount++;
		}
	}
	if (ShipCount > 0)
	{
		// average cohesion force of Ships
		AveragePosition /= ShipCount;
		Steering = AveragePosition - this->GetActorLocation();
		Steering.GetSafeNormal() -= this->PredatorVelocity.GetSafeNormal();
		Steering *= CenteringStrength;
		return Steering;
	}
	else
	{
		return FVector::ZeroVector;
	}
}

void APredator::FlightPath(float DeltaTime)
{
	FVector Acceleration = FVector::ZeroVector;

	//update position and rotation
	this->SetActorLocation(this->GetActorLocation() + (PredatorVelocity * DeltaTime));
	this->SetActorRotation(PredatorVelocity.ToOrientationQuat());

	TArray<AActor*> NearbyShips;
	PerceptionSensor->GetOverlappingActors(NearbyShips, TSubclassOf<APredator>());
	Acceleration += AvoidShips(NearbyShips);
	Acceleration += VelocityMatching(NearbyShips);
	Acceleration += FlockCentering(NearbyShips);

	// tracking boids
	TArray<AActor*> NearbyBoids;
	PerceptionSensor->GetOverlappingActors(NearbyBoids, TSubclassOf<ABoid>());
	// if not attacking
	if (!IsAttacking)
	{
		TrackBoids(NearbyBoids);
		if (TargetBoid)
		{
			FVector Direction = TargetBoid->GetActorLocation() - GetActorLocation();
			Direction.Normalize();
			//Update position based on direction and move MoveSpeed
			FVector NewPos = GetActorLocation() + Direction * PredatorSpeed * DeltaTime;
			SetActorLocation(NewPos);
		}
	}
	// if attacking
	else
	{
		// pause 5s
		if (FDateTime::UtcNow().ToUnixTimestamp() > 5)
		{
			IsAttacking = false;
		}
	}
	
	if (IsObstacleAhead())
		Acceleration += AvoidObstacle();

	//apply gas cloud forces
	for (int i = 0; i < PredatorSpawner->GasClouds.Num(); i++)
	{
		FVector Force = PredatorSpawner->GasClouds[i]->GetActorLocation() - GetActorLocation();
		if (Force.Size() < 1500)
			GasCloudForces.Add(Force);
	}

	for (FVector GasCloudForce : GasCloudForces)
	{
		GasCloudForce = GasCloudForce.GetSafeNormal() * GasCloudStrength;
		Acceleration += GasCloudForce;
		GasCloudForces.Remove(GasCloudForce);
	}

	//update velocity
	PredatorVelocity += (Acceleration * DeltaTime);
	PredatorVelocity = PredatorVelocity.GetClampedToSize(MinSpeed, MaxSpeed);
}

bool APredator::IsObstacleAhead()
{
	if (AvoidanceSensors.Num() > 0)
	{
		FQuat SensorRotation = FQuat::FindBetweenVectors(AvoidanceSensors[0], this->GetActorForwardVector());
		FVector NewSensorDirection = FVector::ZeroVector;
		NewSensorDirection = SensorRotation.RotateVector(AvoidanceSensors[0]);
		FCollisionQueryParams TraceParameters;
		FHitResult Hit;
		//line trace
		GetWorld()->LineTraceSingleByChannel(Hit,
		                                     this->GetActorLocation(),
		                                     this->GetActorLocation() + NewSensorDirection * SensorRadius,
		                                     ECC_GameTraceChannel1,
		                                     TraceParameters);

		//check if Predator is inside object (i.e. no need to avoid/impossible to)
		if (Hit.bBlockingHit)
		{
			TArray<AActor*> OverlapActors;
			PredatorCollision->GetOverlappingActors(OverlapActors);
			for (AActor* OverlapActor : OverlapActors)
			{
				if (Hit.Actor == OverlapActor)
				{
					return false;
				}
			}
		}
		return Hit.bBlockingHit;
	}
	return false;
}

FVector APredator::AvoidObstacle()
{
	FVector Steering = FVector::ZeroVector;
	FQuat SensorRotation = FQuat::FindBetweenVectors(AvoidanceSensors[0], this->GetActorForwardVector());
	FVector NewSensorDirection = FVector::ZeroVector;
	FCollisionQueryParams TraceParameters;
	FHitResult Hit;

	for (FVector AvoidanceSensor : AvoidanceSensors)
	{
		// Trace check
		NewSensorDirection = SensorRotation.RotateVector(AvoidanceSensor);
		GetWorld()->LineTraceSingleByChannel(
			Hit,
			this->GetActorLocation(),
			this->GetActorLocation() + NewSensorDirection * SensorRadius,
			ECC_GameTraceChannel1,
			TraceParameters);

		if (!Hit.bBlockingHit)
		{
			Steering = NewSensorDirection.GetSafeNormal() - this->PredatorVelocity.GetSafeNormal();
			Steering *= AvoidanceStrength;
			return Steering;
		}
	}
	return FVector::ZeroVector;
}


void APredator::TrackBoids(TArray<AActor*> NearbyShips)
{
	for (AActor* OverlapActor : NearbyShips)
	{
		ABoid* NearbyBoidShip = Cast<ABoid>(OverlapActor);
		if (NearbyBoidShip != nullptr && NearbyBoidShip->IsAlive)
		{
			// check if LocalShip is outside perception fov
			if (FVector::DotProduct(this->GetActorForwardVector(),
			                        (NearbyBoidShip->GetActorLocation() - this->GetActorLocation()).GetSafeNormal()) <=
				SeparationFOV)
			{
				continue; //LocalShip is outside the perception, disregard it and continue the loop
			}

			if (TargetBoid)
			{
				// Check distance from current location to current stone resource and new stone resource
				if (FVector::Dist(NearbyBoidShip->GetActorLocation(), GetActorLocation()) <
					FVector::Dist(TargetBoid->GetActorLocation(), GetActorLocation()))
				{
					// If shorter, have new closest
					TargetBoid = NearbyBoidShip;
				}
			}
			else
			{
				TargetBoid = NearbyBoidShip;
			}
		}
	}
}

void APredator::OnHitboxOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                     UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep,
                                     const FHitResult& SweepResult)
{
	if (OtherActor && OtherActor != this)
	{
		// hit other Predators
		if (Invincibility <= 0 &&
			OtherComponent->GetName().Equals(TEXT("Predator Collision Component")))
		{
			AGasCloud* cloud = Cast<AGasCloud>(OtherActor);
			if (cloud != nullptr)
			{
				CollisionCloud = cloud;
				return;
			}

			APredator* ship = Cast<APredator>(OtherActor);
			if (ship != nullptr)
			{
				PredatorSpawner->NumOfShips--;
				// Predator dead
				IsAlive = false;
				// when alteration equal to 1, it means killed by hitting other Predator
				CalculateAndStoreFitness(1);
				PredatorSpawner->deadDNA.Add(ShipDNA);
				Destroy();
				return;
			}
		}
		// hit Cube Wall
		if (OtherActor->GetName().Contains("Cube") &&
			OverlappedComponent->GetName().Equals(TEXT("Predator Collision Component")))
		{
			PredatorSpawner->NumOfShips--;
			// when alteration equal to 2, it means killed by hitting Wall
			CalculateAndStoreFitness(2);
			PredatorSpawner->deadDNA.Add(ShipDNA);
			IsAlive = false;
			Destroy();
		}
		// hit Boids
		if (OtherActor->GetName().Contains("HarvestShip") &&
			OtherComponent->GetName().Equals(TEXT("Predator Collision Component")))
		{
			AGasCloud* cloud = Cast<AGasCloud>(OtherActor);
			if (cloud != nullptr)
			{
				CollisionCloud = cloud;
				return;
			}
			ABoid* ship = Cast<ABoid>(OtherActor);
			if (ship != nullptr)
			{
				Spawner->NumOfShips --;
				// after hit boid, collect all the gold of it and clear the boid gold num.
				GoldCollected = ship->GoldCollected;
				ship->GoldCollected = 0;
				// when alteration equal to 3, it means killed by hitting other Predator
				CalculateAndStoreFitness(3);
				return;
			}
		}
	}
}

void APredator::OnHitboxOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                   UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex)
{
	AGasCloud* cloud = Cast<AGasCloud>(OtherActor);
	if (cloud != nullptr)
	{
		CollisionCloud = nullptr;
	}
}

void APredator::SetDNA()
{
	VelocityStrength = ShipDNA.StrengthValues[0];
	SeparationStrength = ShipDNA.StrengthValues[1];
	CenteringStrength = ShipDNA.StrengthValues[2];
	AvoidanceStrength = ShipDNA.StrengthValues[3];
	GasCloudStrength = ShipDNA.StrengthValues[4];
}

void APredator::CalculateAndStoreFitness(int alteration)
{
	if (ShipDNA.m_storedFitness == -1)
	{
		// first time calculating fitness
		ShipDNA.m_storedFitness = 0;

		// Add Fitness calculation Here
		// if hit other boids, it will receive a 25% reduction in fitness.
		if (alteration == 1)
			ShipDNA.m_storedFitness -= ShipDNA.m_storedFitness * 0.25;
			// if hit walls, it will receive a 25% reduction in fitness.
		else if (alteration == 2)
			ShipDNA.m_storedFitness -= ShipDNA.m_storedFitness * 0.25;
			// if hit boids, it will increase 50 fitness.
		else if (alteration == 3)
			ShipDNA.m_storedFitness += 50.0f;
		Fitness = ShipDNA.m_storedFitness;
	}
	// if already calculate
	else
	{
		// hit boids.
		if (alteration == 3)
			ShipDNA.m_storedFitness += 50.0f;
		Fitness = ShipDNA.m_storedFitness;
	}
}
