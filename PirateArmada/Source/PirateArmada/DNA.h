// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class PIRATEARMADA_API DNA
{
public:
	DNA();
	DNA(int DNASize);
	~DNA();

	int NumOfStrengthValues;
	TArray<float> StrengthValues;
	float MUTATION_CHANCE = 0.05f;
	int m_storedFitness = -1;

	DNA Crossover(DNA otherDNA);
	void Mutation();
	
};
