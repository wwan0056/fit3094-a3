// Fill out your copyright notice in the Description page of Project Settings.


#include "DNA.h"

DNA::DNA()
{
}

DNA::DNA(int DNASize)
{
	NumOfStrengthValues = DNASize;
	for (int i = 0; i < NumOfStrengthValues; i++)
	{
		StrengthValues.Add(FMath::RandRange(0.0f, 10000.0f));
	}
}

DNA::~DNA()
{
}

DNA DNA::Crossover(DNA otherDNA)
{
	DNA retVal = DNA(NumOfStrengthValues);

	const int minVal = 2;
	const int midIndex = FMath::RandRange(minVal, NumOfStrengthValues - minVal);

	for (int i = 0; i < NumOfStrengthValues; ++i)
	{
		if (i < midIndex)
		{
			retVal.StrengthValues[i] = StrengthValues[i];
		}
		else
		{
			retVal.StrengthValues[i] = otherDNA.StrengthValues[i];
		}
	}
	return retVal;
}

void DNA::Mutation()
{
	// array from 0 - 100,000
	for (int i = 0; i < NumOfStrengthValues; ++i)
	{
		if (FMath::RandRange(0.0f, 1.0f) < MUTATION_CHANCE)
		{
			// You could improve this code here...
			StrengthValues[i] = FMath::RandRange(0, 100000);
		}
	}
}
