// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Predator.h"
#include "GameFramework/Actor.h"
#include "PredatorSpawner.generated.h"

UCLASS()
class PIRATEARMADA_API APredatorSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APredatorSpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	UPROPERTY(EditAnywhere, Category = "Entities")
	float MaxShipCount = 15;

	UPROPERTY(EditAnywhere, Category = "Entities")
	TSubclassOf<APredator> PirateShip;
	
	UPROPERTY(EditAnywhere, Category = "Entities")
	TSubclassOf<AGasCloud> GasCloud;
	
	int NumOfShips = 0;
	TArray<AGasCloud*> GasClouds;
	
	void SpawnShip();
	void SetShipVariables(APredator* Ship);
	
	TArray<DNA> m_population;
	TArray<DNA> deadDNA;

	int m_numberOfGenerations=0;
	int m_highestFitness = 0;
	int NUM_PARENTS_PAIR = 5;
	float MUTATION_CHANCE = 0.10f;
	void GeneratePopulation(TArray<DNA> newChildren = TArray<DNA>());
	TArray<DNA> ChildGeneration();
};
