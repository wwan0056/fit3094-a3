// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef PIRATEARMADA_Predator_generated_h
#error "Predator.generated.h already included, missing '#pragma once' in Predator.h"
#endif
#define PIRATEARMADA_Predator_generated_h

#define PirateArmada_Source_PirateArmada_Predator_h_18_SPARSE_DATA
#define PirateArmada_Source_PirateArmada_Predator_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHitboxOverlapEnd); \
	DECLARE_FUNCTION(execOnHitboxOverlapBegin);


#define PirateArmada_Source_PirateArmada_Predator_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHitboxOverlapEnd); \
	DECLARE_FUNCTION(execOnHitboxOverlapBegin);


#define PirateArmada_Source_PirateArmada_Predator_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPredator(); \
	friend struct Z_Construct_UClass_APredator_Statics; \
public: \
	DECLARE_CLASS(APredator, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PirateArmada"), NO_API) \
	DECLARE_SERIALIZER(APredator)


#define PirateArmada_Source_PirateArmada_Predator_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAPredator(); \
	friend struct Z_Construct_UClass_APredator_Statics; \
public: \
	DECLARE_CLASS(APredator, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PirateArmada"), NO_API) \
	DECLARE_SERIALIZER(APredator)


#define PirateArmada_Source_PirateArmada_Predator_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APredator(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APredator) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APredator); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APredator); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APredator(APredator&&); \
	NO_API APredator(const APredator&); \
public:


#define PirateArmada_Source_PirateArmada_Predator_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APredator(APredator&&); \
	NO_API APredator(const APredator&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APredator); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APredator); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APredator)


#define PirateArmada_Source_PirateArmada_Predator_h_18_PRIVATE_PROPERTY_OFFSET
#define PirateArmada_Source_PirateArmada_Predator_h_15_PROLOG
#define PirateArmada_Source_PirateArmada_Predator_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PirateArmada_Source_PirateArmada_Predator_h_18_PRIVATE_PROPERTY_OFFSET \
	PirateArmada_Source_PirateArmada_Predator_h_18_SPARSE_DATA \
	PirateArmada_Source_PirateArmada_Predator_h_18_RPC_WRAPPERS \
	PirateArmada_Source_PirateArmada_Predator_h_18_INCLASS \
	PirateArmada_Source_PirateArmada_Predator_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PirateArmada_Source_PirateArmada_Predator_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PirateArmada_Source_PirateArmada_Predator_h_18_PRIVATE_PROPERTY_OFFSET \
	PirateArmada_Source_PirateArmada_Predator_h_18_SPARSE_DATA \
	PirateArmada_Source_PirateArmada_Predator_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	PirateArmada_Source_PirateArmada_Predator_h_18_INCLASS_NO_PURE_DECLS \
	PirateArmada_Source_PirateArmada_Predator_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PIRATEARMADA_API UClass* StaticClass<class APredator>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PirateArmada_Source_PirateArmada_Predator_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
