// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PIRATEARMADA_ShipSpawner_generated_h
#error "ShipSpawner.generated.h already included, missing '#pragma once' in ShipSpawner.h"
#endif
#define PIRATEARMADA_ShipSpawner_generated_h

#define PirateArmada_Source_PirateArmada_ShipSpawner_h_14_SPARSE_DATA
#define PirateArmada_Source_PirateArmada_ShipSpawner_h_14_RPC_WRAPPERS
#define PirateArmada_Source_PirateArmada_ShipSpawner_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define PirateArmada_Source_PirateArmada_ShipSpawner_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAShipSpawner(); \
	friend struct Z_Construct_UClass_AShipSpawner_Statics; \
public: \
	DECLARE_CLASS(AShipSpawner, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PirateArmada"), NO_API) \
	DECLARE_SERIALIZER(AShipSpawner)


#define PirateArmada_Source_PirateArmada_ShipSpawner_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAShipSpawner(); \
	friend struct Z_Construct_UClass_AShipSpawner_Statics; \
public: \
	DECLARE_CLASS(AShipSpawner, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PirateArmada"), NO_API) \
	DECLARE_SERIALIZER(AShipSpawner)


#define PirateArmada_Source_PirateArmada_ShipSpawner_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AShipSpawner(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AShipSpawner) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShipSpawner); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShipSpawner); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShipSpawner(AShipSpawner&&); \
	NO_API AShipSpawner(const AShipSpawner&); \
public:


#define PirateArmada_Source_PirateArmada_ShipSpawner_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AShipSpawner(AShipSpawner&&); \
	NO_API AShipSpawner(const AShipSpawner&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AShipSpawner); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AShipSpawner); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AShipSpawner)


#define PirateArmada_Source_PirateArmada_ShipSpawner_h_14_PRIVATE_PROPERTY_OFFSET
#define PirateArmada_Source_PirateArmada_ShipSpawner_h_11_PROLOG
#define PirateArmada_Source_PirateArmada_ShipSpawner_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PirateArmada_Source_PirateArmada_ShipSpawner_h_14_PRIVATE_PROPERTY_OFFSET \
	PirateArmada_Source_PirateArmada_ShipSpawner_h_14_SPARSE_DATA \
	PirateArmada_Source_PirateArmada_ShipSpawner_h_14_RPC_WRAPPERS \
	PirateArmada_Source_PirateArmada_ShipSpawner_h_14_INCLASS \
	PirateArmada_Source_PirateArmada_ShipSpawner_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PirateArmada_Source_PirateArmada_ShipSpawner_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PirateArmada_Source_PirateArmada_ShipSpawner_h_14_PRIVATE_PROPERTY_OFFSET \
	PirateArmada_Source_PirateArmada_ShipSpawner_h_14_SPARSE_DATA \
	PirateArmada_Source_PirateArmada_ShipSpawner_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	PirateArmada_Source_PirateArmada_ShipSpawner_h_14_INCLASS_NO_PURE_DECLS \
	PirateArmada_Source_PirateArmada_ShipSpawner_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PIRATEARMADA_API UClass* StaticClass<class AShipSpawner>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PirateArmada_Source_PirateArmada_ShipSpawner_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
