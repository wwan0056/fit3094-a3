// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PirateArmada/PirateArmadaGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePirateArmadaGameModeBase() {}
// Cross Module References
	PIRATEARMADA_API UClass* Z_Construct_UClass_APirateArmadaGameModeBase_NoRegister();
	PIRATEARMADA_API UClass* Z_Construct_UClass_APirateArmadaGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_PirateArmada();
// End Cross Module References
	void APirateArmadaGameModeBase::StaticRegisterNativesAPirateArmadaGameModeBase()
	{
	}
	UClass* Z_Construct_UClass_APirateArmadaGameModeBase_NoRegister()
	{
		return APirateArmadaGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_APirateArmadaGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_APirateArmadaGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_PirateArmada,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_APirateArmadaGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "PirateArmadaGameModeBase.h" },
		{ "ModuleRelativePath", "PirateArmadaGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_APirateArmadaGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<APirateArmadaGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_APirateArmadaGameModeBase_Statics::ClassParams = {
		&APirateArmadaGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_APirateArmadaGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_APirateArmadaGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_APirateArmadaGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_APirateArmadaGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(APirateArmadaGameModeBase, 1905580776);
	template<> PIRATEARMADA_API UClass* StaticClass<APirateArmadaGameModeBase>()
	{
		return APirateArmadaGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_APirateArmadaGameModeBase(Z_Construct_UClass_APirateArmadaGameModeBase, &APirateArmadaGameModeBase::StaticClass, TEXT("/Script/PirateArmada"), TEXT("APirateArmadaGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(APirateArmadaGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
