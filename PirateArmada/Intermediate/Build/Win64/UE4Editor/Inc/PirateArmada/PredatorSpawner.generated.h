// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PIRATEARMADA_PredatorSpawner_generated_h
#error "PredatorSpawner.generated.h already included, missing '#pragma once' in PredatorSpawner.h"
#endif
#define PIRATEARMADA_PredatorSpawner_generated_h

#define PirateArmada_Source_PirateArmada_PredatorSpawner_h_13_SPARSE_DATA
#define PirateArmada_Source_PirateArmada_PredatorSpawner_h_13_RPC_WRAPPERS
#define PirateArmada_Source_PirateArmada_PredatorSpawner_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define PirateArmada_Source_PirateArmada_PredatorSpawner_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPredatorSpawner(); \
	friend struct Z_Construct_UClass_APredatorSpawner_Statics; \
public: \
	DECLARE_CLASS(APredatorSpawner, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PirateArmada"), NO_API) \
	DECLARE_SERIALIZER(APredatorSpawner)


#define PirateArmada_Source_PirateArmada_PredatorSpawner_h_13_INCLASS \
private: \
	static void StaticRegisterNativesAPredatorSpawner(); \
	friend struct Z_Construct_UClass_APredatorSpawner_Statics; \
public: \
	DECLARE_CLASS(APredatorSpawner, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/PirateArmada"), NO_API) \
	DECLARE_SERIALIZER(APredatorSpawner)


#define PirateArmada_Source_PirateArmada_PredatorSpawner_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APredatorSpawner(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APredatorSpawner) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APredatorSpawner); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APredatorSpawner); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APredatorSpawner(APredatorSpawner&&); \
	NO_API APredatorSpawner(const APredatorSpawner&); \
public:


#define PirateArmada_Source_PirateArmada_PredatorSpawner_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APredatorSpawner(APredatorSpawner&&); \
	NO_API APredatorSpawner(const APredatorSpawner&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APredatorSpawner); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APredatorSpawner); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APredatorSpawner)


#define PirateArmada_Source_PirateArmada_PredatorSpawner_h_13_PRIVATE_PROPERTY_OFFSET
#define PirateArmada_Source_PirateArmada_PredatorSpawner_h_10_PROLOG
#define PirateArmada_Source_PirateArmada_PredatorSpawner_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PirateArmada_Source_PirateArmada_PredatorSpawner_h_13_PRIVATE_PROPERTY_OFFSET \
	PirateArmada_Source_PirateArmada_PredatorSpawner_h_13_SPARSE_DATA \
	PirateArmada_Source_PirateArmada_PredatorSpawner_h_13_RPC_WRAPPERS \
	PirateArmada_Source_PirateArmada_PredatorSpawner_h_13_INCLASS \
	PirateArmada_Source_PirateArmada_PredatorSpawner_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PirateArmada_Source_PirateArmada_PredatorSpawner_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PirateArmada_Source_PirateArmada_PredatorSpawner_h_13_PRIVATE_PROPERTY_OFFSET \
	PirateArmada_Source_PirateArmada_PredatorSpawner_h_13_SPARSE_DATA \
	PirateArmada_Source_PirateArmada_PredatorSpawner_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	PirateArmada_Source_PirateArmada_PredatorSpawner_h_13_INCLASS_NO_PURE_DECLS \
	PirateArmada_Source_PirateArmada_PredatorSpawner_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PIRATEARMADA_API UClass* StaticClass<class APredatorSpawner>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PirateArmada_Source_PirateArmada_PredatorSpawner_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
