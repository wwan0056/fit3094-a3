// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PIRATEARMADA_PirateArmadaGameModeBase_generated_h
#error "PirateArmadaGameModeBase.generated.h already included, missing '#pragma once' in PirateArmadaGameModeBase.h"
#endif
#define PIRATEARMADA_PirateArmadaGameModeBase_generated_h

#define PirateArmada_Source_PirateArmada_PirateArmadaGameModeBase_h_15_SPARSE_DATA
#define PirateArmada_Source_PirateArmada_PirateArmadaGameModeBase_h_15_RPC_WRAPPERS
#define PirateArmada_Source_PirateArmada_PirateArmadaGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define PirateArmada_Source_PirateArmada_PirateArmadaGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPirateArmadaGameModeBase(); \
	friend struct Z_Construct_UClass_APirateArmadaGameModeBase_Statics; \
public: \
	DECLARE_CLASS(APirateArmadaGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/PirateArmada"), NO_API) \
	DECLARE_SERIALIZER(APirateArmadaGameModeBase)


#define PirateArmada_Source_PirateArmada_PirateArmadaGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPirateArmadaGameModeBase(); \
	friend struct Z_Construct_UClass_APirateArmadaGameModeBase_Statics; \
public: \
	DECLARE_CLASS(APirateArmadaGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/PirateArmada"), NO_API) \
	DECLARE_SERIALIZER(APirateArmadaGameModeBase)


#define PirateArmada_Source_PirateArmada_PirateArmadaGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APirateArmadaGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APirateArmadaGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APirateArmadaGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APirateArmadaGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APirateArmadaGameModeBase(APirateArmadaGameModeBase&&); \
	NO_API APirateArmadaGameModeBase(const APirateArmadaGameModeBase&); \
public:


#define PirateArmada_Source_PirateArmada_PirateArmadaGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APirateArmadaGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APirateArmadaGameModeBase(APirateArmadaGameModeBase&&); \
	NO_API APirateArmadaGameModeBase(const APirateArmadaGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APirateArmadaGameModeBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APirateArmadaGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APirateArmadaGameModeBase)


#define PirateArmada_Source_PirateArmada_PirateArmadaGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define PirateArmada_Source_PirateArmada_PirateArmadaGameModeBase_h_12_PROLOG
#define PirateArmada_Source_PirateArmada_PirateArmadaGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PirateArmada_Source_PirateArmada_PirateArmadaGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	PirateArmada_Source_PirateArmada_PirateArmadaGameModeBase_h_15_SPARSE_DATA \
	PirateArmada_Source_PirateArmada_PirateArmadaGameModeBase_h_15_RPC_WRAPPERS \
	PirateArmada_Source_PirateArmada_PirateArmadaGameModeBase_h_15_INCLASS \
	PirateArmada_Source_PirateArmada_PirateArmadaGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define PirateArmada_Source_PirateArmada_PirateArmadaGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	PirateArmada_Source_PirateArmada_PirateArmadaGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	PirateArmada_Source_PirateArmada_PirateArmadaGameModeBase_h_15_SPARSE_DATA \
	PirateArmada_Source_PirateArmada_PirateArmadaGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	PirateArmada_Source_PirateArmada_PirateArmadaGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	PirateArmada_Source_PirateArmada_PirateArmadaGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PIRATEARMADA_API UClass* StaticClass<class APirateArmadaGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID PirateArmada_Source_PirateArmada_PirateArmadaGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
